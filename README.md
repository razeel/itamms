# ITAMMS Coding Challenge

## Overview

IT Assets Management Made Simple (alias ITAMMS), is a coding challenge work.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) which is Facebook's official boilerplate to kick-off projects in plug-play speed.


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

## Setup Firebase

* Create a Firebase project
* At Firebase dashboard, there you can find a menu item which says **Authentication**
* Select it and click **Sign-In Method** menu item afterward
* Enable the authentication with Email/Password
* Find your configuration in the project settings on your dashboard which is top right corner **WEB SETUP**
*  There you have access to all the necessary information: secrets, keys, ids and other properties
* You will copy these and replace it with a file `./src/config/firebase.js` with your own values

```
./src/config
├── firebase.js
└── routes.js
```

## Features

* React
* Firebase for back-end-as-service
* React-Router, Connected-React-Route
* Redux, Redux-Thunk
* Redux-Form, React-Table
* Lodash

## Author

ITAMMS