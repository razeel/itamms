import React, { Component } from 'react';
import { connect } from 'react-redux'
import { values } from 'lodash';

// config
import RoutesConfig from '../config/routes';

// components
import ProtectedComponent from './Protected';
import Navigation from './Navigation';
import Table from './Table';

// action creators
import { doLogout, getAssets } from '../store/actions/firebase';

// styles
import 'react-table/react-table.css';

// 
let Dashboard = class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.props.getAssets();
  }

  logoutHandler = () => {
    this.props.doLogout(RoutesConfig.LOGIN);
  }

  render() {

    const { crud, firebase: { assets }, status: { loggedIn } } = this.props;

    const columns = [{
      Header: 'Label',
      accessor: 'label',
    }, {
      Header: 'Type',
      accessor: 'type'
    }, {
      Header: 'Description',
      accessor: 'description'
    }, {
      Header: 'Status',
      accessor: 'status'
    }, {
      Header: 'Owner',
      accessor: 'current_owner'
    }, {
      Header: 'Purchased',
      accessor: 'purchased_date'
    }];

    return (
      <React.Fragment>

        {
          loggedIn
          &&
          <Navigation />
        }

        <h1>Dashboard</h1>
        {
          crud === 'TABLE'
          &&
          <Table tableData={values(assets)} columns={columns} />
        }

      </React.Fragment>
    )
  }

}

const mapStateToProps = ({status, firebase}) => ({
  status,
  firebase
});

Dashboard = connect(mapStateToProps, { doLogout, getAssets })(Dashboard);

export default ProtectedComponent(Dashboard);