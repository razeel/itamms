import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form';

// actions
import { doLogin } from '../../store/actions/firebase'


let LoginPage = class LoginPage extends Component {

  submit = (values) => {
    const { email, password } = values;
    const { redirectTo, pathname } = this.props;
    const isRedirect = redirectTo !== pathname;
    this.props.doLogin(email, password, isRedirect);
  }

  render() {

    const {
      status: { loginError },
      loginForm,
      handleSubmit,
      reset,
      pristine,
      submitting
    } = this.props;

    if (!loginForm) return null;

    return (
      
      <Fragment>

      <h1>Login</h1>
      
      {
        (loginError && loginForm.submitSucceeded)
        &&
        <div className="error">{loginError}</div>
      }

      <form onSubmit={handleSubmit(props => this.submit(props))}>

        <div>
          <label>Email</label>
          <div>
            <Field
              name="email"
              component="input"
              type="email"
              placeholder="Email"
            />
          </div>
        </div>

        <div>
          <label>Password</label>
          <div>
            <Field
              name="password"
              component="input"
              type="password"
              placeholder="Password"
            />
          </div>
        </div>

        <div>
          <button type="submit" disabled={pristine || submitting}>Submit</button>
          <button type="button" disabled={pristine || submitting} onClick={reset}>Reset</button>
        </div>
          

      </form>
      </Fragment>
    );
  }
};

// mapStateToProps
const mapStateToProps = ({router, status, form}) => {
  const { location: { pathname } } = router;
  const { loginForm } = form;
  return {
    status,
    pathname,
    loginForm
  }
};

// reduxForm validation
const validate = (values) => {
  const errors = { };
  if (!values.email) errors.email = 'Email address is required';
  if (!values.password) errors.password = 'Password is required';

  return errors;
}

// reduxForm decorator
LoginPage = reduxForm({
  form: 'loginForm',
  validate
})(LoginPage);

// connect
LoginPage = connect(mapStateToProps, { doLogin })(LoginPage);

export default LoginPage;