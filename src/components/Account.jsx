import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

// config
import RoutesConfig from '../config/routes';

// components
import ProtectedComponent from './Protected';

// 
let AccountPage = class AccountPage extends Component {

  render() {
    const { status: { user } } = this.props;
    return (
      <div>
        <h1>My Account</h1>
        <Link to={RoutesConfig.DASHBOARD}>Go to Dashboard</Link>

        <ul>
          <li>Email: {user.email}</li>
          <li>UID: {user.uid}</li>
        </ul>
      </div>
    )
  }

}

const mapStateToProps = ({status}) => ({
  status
});

AccountPage = connect(mapStateToProps, {})(AccountPage);

export default ProtectedComponent(AccountPage);