import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

// config
import RoutesConfig from '../../config/routes';

// components
import ProtectedComponent from '../Protected';

// actions
import { installSampleData } from '../../store/actions/install';

let Install = class Install extends Component {

  constructor(props) {
    super(props);
    this.props.installSampleData();
  }

  render() {

    const { status: { isSampleData } } = this.props;

    return (
      <div>
        {
          isSampleData
          &&
          <React.Fragment>
            <h2>Sampled Data Exists</h2>
            <Link to={RoutesConfig.DASHBOARD}>Go to Dashboard</Link>
          </React.Fragment>
        }
        {
          !isSampleData
          &&
          <h2>Installing Sample Data...</h2>
        }
      </div>
    )
  }

}

const mapStateToProps = ({status}) => ({
  status
});

Install = connect(mapStateToProps, { installSampleData })(Install);

export default ProtectedComponent(Install);