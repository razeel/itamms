import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'

// config
import RoutesConfig from '../config/routes'

// actions
import { checkAuth } from '../store/actions/firebase';

// components
import Loading from './Loading';
import LoginPage from './Auth/Login';

const ProtectedRoute = (WrappedComponent) => {

  class AuthHandler extends Component {

    constructor(props) {
      super(props);

      this.authPages = [RoutesConfig.LOGIN, RoutesConfig.REGISTER, RoutesConfig.FORGOTTEN];
      this.isAuthPage = this.authPages.indexOf(props.currentURL) !== -1;
    }
    
    render() {

      const { loggedIn, loading, currentURL } = this.props;

      if(loading) return <Loading message="Loading" />

      if (loggedIn && !this.isAuthPage) return <WrappedComponent {...this.props} />;
      if (!loggedIn && this.isAuthPage) return <WrappedComponent {...this.props} />;

      if (loggedIn && this.isAuthPage) return <Redirect to={RoutesConfig.ACCOUNT} />

      return <LoginPage redirectTo={currentURL} />;
    }
  }

  const mapStateToProps = ({router, status}) => {
    const { loggedIn, loading } = status;
    const { location: { pathname } } = router;
    return {
      loggedIn,
      loading,
      currentURL: pathname
    };
  }

  return connect(mapStateToProps, { checkAuth })(AuthHandler);
};

export default ProtectedRoute;
