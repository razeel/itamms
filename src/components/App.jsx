// modules
import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

// components
import Dashboard from './Dashboard';
import Login from './Auth/Login';
import Account from './Account';
import Demo from './Demo/Install';

// actions
import { checkAuth } from '../store/actions/firebase'

// configs
import RoutesConfig from '../config/routes';

class App extends Component {

  constructor(props) {
    super(props);

    props.checkAuth();
  }

  render() {
    return (
        <div className="main">

        <Switch>

          <Route
            exact
            path='/install-sample-data'
            component={Demo}
          />

          <Route
            exact
            path={RoutesConfig.DASHBOARD}
            render={(props) => <Dashboard crud="TABLE" {...props} />}
          />

          <Route
            exact
            path={`${RoutesConfig.DASHBOARD}/add`}
            render={(props) => <Dashboard crud="CREATE" {...props} />}
          />

          <Route
            exact
            path={`${RoutesConfig.DASHBOARD}/update/:uid`}
            render={(props) => <Dashboard crud="UPDATE" {...props} />}
          />

          <Route
            exact
            path={RoutesConfig.LOGIN}
            component={Login}
          />

          <Route
            exact
            path={RoutesConfig.ACCOUNT}
            component={Account}
          />
        </Switch>

        </div>
    );
  }
}

const mapStateToProps = ({status}) => ({
  status
});

export default withRouter(connect(mapStateToProps, { checkAuth })(App));
