import React, { Component } from 'react';
import ReactTable from 'react-table';

let CrudTable = class CrudTable extends Component {

  render() {

    const {
      key,
      tableData,
      columns,
      filterable,
      sortable,
      resizable,
      defaultPageSize
    } = this.props;

    return (
      <ReactTable 
        key={(key) ? key : 'table'}
        data={tableData}
        columns={columns}
        filterable={(filterable) ? filterable : true}
        sortable={(sortable) ? sortable : true}
        resizable={(resizable) ? resizable : true}
        defaultPageSize={defaultPageSize ? defaultPageSize : 10}
        defaultFilterMethod={filterCaseInsensitive}
      />
    )
  }

}

// util methods
export const filterCaseInsensitive = (filter, row) => {
  const id = filter.pivotId || filter.id;
  const regEx = new RegExp(`${filter.value.toLowerCase()}`);
  return (
    row[id] !== undefined ? regEx.test(String(row[id].toLowerCase())) : true
  );
};

export default CrudTable;