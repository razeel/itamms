import React from 'react';

export default (props) => {
  const { message, ellipses } = props;

  let __message__ = [];
  if(message) __message__.push(message);
  if(ellipses !== false) __message__.push('...');

  return (
    <div className="loading-overlay">{__message__.join('')}</div>
  );

};
