import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// components
import ProtectedComponent from './Protected';

// config
import RoutesConfig from '../config/routes';

//  actions
import { doLogout } from '../store/actions/firebase';

let Navigation = class Navigation extends Component {

  logoutHandler = () => {
    this.props.doLogout(RoutesConfig.LOGIN);
  }

  render() {
    return (
      <nav>
        <ul>
          <li><Link to={RoutesConfig.DASHBOARD}>Dashboard</Link></li>
          <li><Link to={RoutesConfig.ACCOUNT}>My Account</Link></li>
          <li><Link to={RoutesConfig.DEMO}>Install Sample Data</Link></li>
          <li><Link to="" onClick={this.logoutHandler}>Logout</Link></li>
        </ul>
      </nav>
    )
  }

}

const mapStateToProps = ({status}) => ({
  status
});

Navigation = connect(mapStateToProps, { doLogout })(Navigation);

export default ProtectedComponent(Navigation);