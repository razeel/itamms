import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import status from './status';
import firebase from './firebase';

export default combineReducers({
  status,
  firebase,
  form: reduxFormReducer
})