const INITIAL_STATE = {
  loading: false,
  assets: {},
  _asset: {},
  _error: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'LOADING_ASSETS': 
      return { ...INITIAL_STATE, loading: true };
    case 'SUCCESS_LOADING_ASSETS':
      return { ...state, assets: action.payload, loading: false, _asset: {}, _error: null }
    case 'FAILED_LOADING_ASSETS':
      return { ...state, assets: {}, loading: false, _asset: {}, _error: action.payload }
    case 'ASSET_CONTEXT':
      return { ...state, _asset: action.payload }
    default:
      return state
  }
}