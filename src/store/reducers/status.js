const INITIAL_STATE = {
  loading: false,
  loggedIn: false,
  loginError: null,
  user: {},
  isSampleData: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'AUTH_CHECKING': 
      return { ...state, loading: true };
    case 'AUTH_SUCCESS':
      return { ...state, loggedIn: true, user: action.payload, loading: false, loginError: null }
    case 'LOGIN_ERROR':
      return { ...state, loginError: action.payload }
    case 'AUTH_FAILED':
      return { ...state, loggedIn: false, loading: false }
    case 'SAMPLE_DATA_EXISTS':
      return { ...state, isSampleData: true }
    default:
      return state
  }
}