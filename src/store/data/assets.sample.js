export default [
  {
    "label": "do nulla",
    "description": "Ea in laboris amet incididunt deserunt culpa nostrud ad eu ex sunt aute. Consequat minim enim eiusmod Lorem in Lorem mollit ex adipisicing voluptate commodo elit officia. Laborum reprehenderit esse in ipsum culpa est laborum officia eiusmod reprehenderit consectetur enim incididunt voluptate.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2018-03-27T03:32:55 -04:00",
    "current_owner": "Trujillo Acosta"
  },
  {
    "label": "ullamco cupidatat",
    "description": "Id in adipisicing consectetur et nisi cupidatat deserunt ullamco nulla elit veniam. Esse est id amet elit eiusmod ea. Tempor ipsum dolore laboris nulla et deserunt ad ut mollit cupidatat incididunt ut incididunt ea.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2016-06-12T03:16:54 -04:00",
    "current_owner": "Huber Charles"
  },
  {
    "label": "dolor velit",
    "description": "Ea ullamco commodo ipsum minim incididunt excepteur incididunt enim elit consectetur eiusmod. Qui elit enim deserunt voluptate nisi anim dolore. Proident culpa duis minim cupidatat nisi et dolor incididunt sit laboris ea tempor. Ut cupidatat magna in ullamco do exercitation ad.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2015-10-08T02:12:11 -04:00",
    "current_owner": "Greta Olson"
  },
  {
    "label": "minim enim",
    "description": "Labore consequat in reprehenderit Lorem officia cupidatat aliqua sunt officia nulla qui do qui. Sint nulla velit cillum et deserunt veniam labore proident irure. Officia proident ipsum minim sint ut. Fugiat consectetur qui reprehenderit tempor eu ad nostrud nulla laboris exercitation magna sit. Velit est commodo culpa deserunt id nostrud sint eiusmod veniam anim eiusmod voluptate reprehenderit. Commodo incididunt anim ipsum eiusmod mollit labore sit eiusmod irure. Consectetur labore commodo nisi dolor velit dolor in.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2015-05-18T07:19:11 -04:00",
    "current_owner": "Arnold Douglas"
  },
  {
    "label": "qui proident",
    "description": "Irure ipsum pariatur veniam fugiat consequat labore ipsum. Laboris sunt non consequat in occaecat enim irure eiusmod eiusmod voluptate mollit esse eiusmod aute. In laboris deserunt laborum dolore ea veniam fugiat enim voluptate officia. Ea exercitation non adipisicing excepteur. Ex labore enim est voluptate laborum. Et velit deserunt mollit commodo eiusmod aliquip duis officia.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2015-07-29T07:14:19 -04:00",
    "current_owner": "Elsie Hurley"
  },
  {
    "label": "tempor ad",
    "description": "Sint veniam eiusmod sint do et Lorem dolor. Amet commodo ut aliquip ad consequat consequat anim officia laborum reprehenderit laborum incididunt nostrud est. Eu magna laborum aliquip nulla labore eiusmod. Tempor reprehenderit dolore nulla proident non cillum tempor nulla proident ipsum ea. Aliquip esse eu minim aliquip amet magna non deserunt voluptate cillum qui cillum. Ullamco minim exercitation incididunt eiusmod eu anim irure.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2015-02-20T11:13:53 -04:00",
    "current_owner": "Stuart Welch"
  },
  {
    "label": "anim consequat",
    "description": "Incididunt eu do cillum ex ut. Qui velit nostrud nulla et sint enim non. Minim exercitation aliquip dolore anim labore elit do ex minim labore amet. Magna ullamco consectetur eu aute incididunt laborum id velit aliquip anim duis occaecat. Nostrud pariatur eu magna Lorem consectetur mollit velit do elit labore proident. Ullamco eiusmod sit officia tempor voluptate anim. Et minim exercitation incididunt culpa amet culpa eu pariatur exercitation magna aute mollit aliquip quis.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2014-04-28T03:45:17 -04:00",
    "current_owner": "Hubbard Goodman"
  },
  {
    "label": "est dolor",
    "description": "Duis adipisicing reprehenderit duis irure dolore non officia et elit laboris. Aliqua deserunt irure est nulla. Veniam nostrud aute exercitation mollit. Magna elit esse aute deserunt. Et anim irure dolore aute in.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2016-08-28T03:59:58 -04:00",
    "current_owner": "Sims Park"
  },
  {
    "label": "est cillum",
    "description": "Aliquip ad sunt deserunt commodo culpa laboris do aute cupidatat dolore aliqua. Sit laboris enim aute irure enim sint sint. Exercitation duis elit irure do aute. Minim adipisicing do incididunt laboris in nisi est minim veniam. Nisi nisi duis ipsum qui incididunt ad magna occaecat amet ullamco.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2018-06-18T12:50:35 -04:00",
    "current_owner": "Suarez Richard"
  },
  {
    "label": "dolor occaecat",
    "description": "Quis incididunt anim labore tempor officia. Do id nisi eiusmod aute magna proident aliquip. Excepteur excepteur cillum consectetur voluptate tempor dolor ullamco esse quis dolore fugiat sunt. Amet deserunt aliqua nostrud mollit mollit et laborum do reprehenderit esse. Exercitation esse non eu ex esse. Do ea veniam anim aute minim eu ex officia ut anim. Adipisicing exercitation aute duis eiusmod ea sunt laboris laborum voluptate excepteur laboris adipisicing.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2016-08-30T06:21:21 -04:00",
    "current_owner": "Twila Dominguez"
  },
  {
    "label": "in aliqua",
    "description": "Ad commodo dolor aliquip reprehenderit ad et exercitation eiusmod deserunt occaecat. Aliquip veniam sunt nisi aliqua Lorem commodo voluptate. Occaecat nostrud nostrud ad incididunt nisi nulla amet. Consectetur nulla anim eu sunt non mollit qui sit irure in est. Veniam ad sit dolore id.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2018-01-12T01:08:24 -04:00",
    "current_owner": "Olive Patterson"
  },
  {
    "label": "excepteur ea",
    "description": "Ad proident amet excepteur magna. Aliqua cupidatat deserunt anim mollit do nostrud magna. Ex irure laboris enim amet consectetur enim adipisicing sunt dolor ad occaecat. Enim commodo fugiat aliquip tempor excepteur anim ad aliqua minim esse. Incididunt et qui sint ullamco fugiat reprehenderit aliqua est. Eiusmod sunt Lorem irure enim sunt excepteur eu consectetur dolor esse nostrud culpa ex sint.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2014-12-06T03:54:50 -04:00",
    "current_owner": "Hester Love"
  },
  {
    "label": "occaecat laborum",
    "description": "Deserunt proident elit aliqua veniam tempor elit. Eu amet duis mollit consectetur tempor. Excepteur sint in velit consequat minim quis aliquip.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2014-11-25T03:56:57 -04:00",
    "current_owner": "Rasmussen Mcdaniel"
  },
  {
    "label": "incididunt id",
    "description": "Nostrud sint veniam magna pariatur consectetur exercitation ex. Amet labore consequat consequat elit qui irure deserunt minim ad veniam. Lorem consequat cillum ipsum elit aliqua reprehenderit sunt do amet. Enim minim qui cupidatat quis commodo aute fugiat et occaecat fugiat. Id velit consequat nostrud qui ad in esse laboris.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2018-01-29T01:04:04 -04:00",
    "current_owner": "Reed Orr"
  },
  {
    "label": "incididunt culpa",
    "description": "Ad consequat do in incididunt ad fugiat nostrud dolor laboris nulla deserunt et quis ex. Veniam enim amet exercitation excepteur. Pariatur aute excepteur aute labore do reprehenderit ipsum occaecat et in amet officia sunt.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2015-07-29T01:05:34 -04:00",
    "current_owner": "Nora Burt"
  },
  {
    "label": "in voluptate",
    "description": "Veniam minim irure cillum et ex. Aute aliqua occaecat officia culpa irure fugiat et fugiat aliqua cillum labore est. Anim ut anim sunt qui pariatur id enim voluptate ut aliquip ullamco incididunt minim. Dolore commodo esse quis deserunt mollit incididunt proident officia tempor deserunt eu aliqua cupidatat mollit. Magna proident minim eu ea dolore labore. Esse eiusmod consectetur dolore in sit pariatur sunt proident eiusmod anim proident qui.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2015-10-30T01:36:28 -04:00",
    "current_owner": "Deborah Henson"
  },
  {
    "label": "reprehenderit esse",
    "description": "Magna sunt reprehenderit voluptate ullamco nulla dolor labore consectetur ea eiusmod esse minim voluptate. Labore amet commodo officia id laboris pariatur eu nisi velit enim eiusmod incididunt sunt. Aliquip non enim do irure dolore voluptate voluptate veniam dolor cupidatat cupidatat. Est nisi incididunt mollit et laborum esse amet velit. Do cupidatat veniam eu sunt anim reprehenderit velit culpa eiusmod deserunt aliqua. Qui anim occaecat ea commodo id consectetur aute. Dolor consequat sit aliqua in reprehenderit cupidatat amet velit nisi voluptate ipsum ad Lorem voluptate.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2016-06-17T04:58:38 -04:00",
    "current_owner": "Francis Randall"
  },
  {
    "label": "eu quis",
    "description": "Ut tempor tempor velit duis in in aute nulla ut esse culpa in nulla. Duis nulla ad pariatur consequat est reprehenderit esse consequat eu nisi ullamco cillum velit dolore. Duis enim nulla reprehenderit occaecat.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2018-04-19T07:54:07 -04:00",
    "current_owner": "Chandra Hampton"
  },
  {
    "label": "consectetur officia",
    "description": "Et aliqua ipsum do deserunt anim enim do id irure est cillum consectetur ea magna. Laborum esse Lorem consectetur officia fugiat mollit adipisicing Lorem. Labore ad tempor ut consequat incididunt irure irure aliquip enim labore id non minim. In laborum ut ea tempor deserunt consequat. Ipsum deserunt consequat exercitation duis veniam magna voluptate cupidatat consequat. Nulla do laborum id occaecat minim id adipisicing aliquip magna incididunt Lorem elit. Est incididunt excepteur mollit consectetur sint ut adipisicing in nulla tempor.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2014-01-02T01:46:51 -04:00",
    "current_owner": "Black Little"
  },
  {
    "label": "nisi adipisicing",
    "description": "Est proident est incididunt pariatur aliqua anim deserunt est tempor dolor ea ut dolor tempor. Esse tempor duis aliqua quis labore qui laboris adipisicing esse et officia eiusmod. Et nisi anim pariatur tempor cillum reprehenderit mollit reprehenderit nisi magna fugiat laboris do. Reprehenderit nisi do ullamco commodo fugiat ullamco exercitation mollit voluptate nostrud mollit nulla. Mollit laborum excepteur laborum exercitation do ut tempor dolor. Non eiusmod ipsum ut mollit aliqua nisi officia non est consequat culpa cupidatat duis. Nulla et ullamco nulla ipsum excepteur voluptate tempor incididunt dolore tempor excepteur cillum velit.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2015-08-12T06:28:42 -04:00",
    "current_owner": "Muriel Mann"
  },
  {
    "label": "ipsum commodo",
    "description": "Cupidatat minim pariatur et fugiat consectetur officia duis dolore duis dolore sit amet. Proident in anim qui nostrud aliqua deserunt. Ad aute excepteur voluptate duis nulla quis fugiat sunt voluptate ullamco ad laborum eu sint. Labore nisi pariatur et amet deserunt exercitation voluptate. Et nulla culpa culpa aliqua deserunt ipsum consectetur. Reprehenderit enim qui id enim ad eu enim occaecat aliqua culpa duis ea in. Lorem labore est ea ea sunt consectetur aute aliquip et dolore.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2015-04-27T04:48:28 -04:00",
    "current_owner": "Sellers Oconnor"
  },
  {
    "label": "sit nostrud",
    "description": "Ut nulla ullamco ipsum excepteur aute. Consequat minim proident anim pariatur ut ut aliqua nostrud dolore mollit aliqua. Cillum anim reprehenderit ullamco reprehenderit eu occaecat. Exercitation fugiat velit amet adipisicing cillum et quis adipisicing. Tempor esse sit nisi Lorem amet in deserunt. Adipisicing sint nisi tempor occaecat do duis est et cupidatat.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2018-03-09T01:01:11 -04:00",
    "current_owner": "Nicholson Vaughan"
  },
  {
    "label": "in ut",
    "description": "Consequat consectetur nulla laborum incididunt nostrud enim reprehenderit fugiat. Velit veniam ut aliquip ad magna sint aliquip nisi ad. Mollit dolore in dolor consectetur laboris eiusmod commodo elit. Nostrud culpa ex id velit aliquip aliqua sint consectetur nostrud occaecat nisi eiusmod id exercitation. Et anim commodo excepteur duis et velit pariatur. Nostrud eiusmod incididunt in magna qui do id sunt dolore in.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2018-02-12T07:15:14 -04:00",
    "current_owner": "Rocha Hopkins"
  },
  {
    "label": "ut exercitation",
    "description": "Adipisicing ex exercitation nisi occaecat. Ut incididunt veniam eiusmod laborum sunt ea ad exercitation velit. Sint sint sint exercitation sunt dolor.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2014-12-04T01:30:37 -04:00",
    "current_owner": "Doreen Harrell"
  },
  {
    "label": "fugiat anim",
    "description": "Tempor veniam eiusmod enim sit labore non. Laborum ut aliquip ad nostrud incididunt voluptate. Magna exercitation anim nostrud aliquip elit duis.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2016-06-28T06:04:22 -04:00",
    "current_owner": "Julianne Berger"
  },
  {
    "label": "sint sit",
    "description": "Duis exercitation sint in qui. Amet id cupidatat non laborum Lorem sint adipisicing eiusmod. Anim nisi qui deserunt commodo amet. Anim aute sit aliqua ex incididunt do labore velit sunt excepteur. Commodo labore cillum qui ut ullamco id id cillum cillum quis ex minim. Irure eu esse velit elit elit magna fugiat fugiat nulla.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2016-05-26T09:22:53 -04:00",
    "current_owner": "Ofelia Glover"
  },
  {
    "label": "quis enim",
    "description": "Sint officia duis nulla labore ad ea esse et consequat exercitation occaecat. Adipisicing in dolore aliqua ad non mollit proident enim cupidatat dolore non nostrud labore. Elit enim do veniam anim reprehenderit mollit nulla non ad dolor esse velit. Qui nisi proident ipsum fugiat est consequat pariatur pariatur. Nulla cupidatat duis aliquip Lorem do dolor duis ea excepteur. Ipsum dolore dolore consectetur qui consectetur ipsum et quis aliquip nisi occaecat minim amet ea.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2015-03-10T07:22:44 -04:00",
    "current_owner": "Rosanne Rutledge"
  },
  {
    "label": "tempor aliquip",
    "description": "Nostrud ex amet mollit duis adipisicing nulla. Ut culpa eu Lorem cillum consectetur reprehenderit irure occaecat ipsum dolore dolor. Culpa aute eu minim aliquip labore cupidatat consectetur voluptate non aliquip Lorem incididunt veniam velit.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2014-04-26T10:19:48 -04:00",
    "current_owner": "Whitaker Harmon"
  },
  {
    "label": "dolor velit",
    "description": "Labore fugiat reprehenderit nostrud reprehenderit enim minim reprehenderit non est. Dolor quis ex qui commodo anim. Laborum ea cillum quis do mollit nulla occaecat incididunt fugiat pariatur laboris fugiat anim. Et amet est incididunt ad labore cillum. Tempor in laborum laborum qui consequat Lorem tempor. Nisi aliqua adipisicing eu in nulla exercitation. Fugiat dolore proident reprehenderit velit aute aliqua nulla minim minim dolore mollit.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2017-06-06T08:53:20 -04:00",
    "current_owner": "Eloise Palmer"
  },
  {
    "label": "exercitation voluptate",
    "description": "Labore sint cupidatat duis deserunt. Id anim commodo mollit non aliqua. Ullamco mollit irure do mollit culpa reprehenderit velit nulla elit.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2017-07-04T04:08:37 -04:00",
    "current_owner": "Good Everett"
  },
  {
    "label": "duis cillum",
    "description": "Culpa minim non velit eiusmod tempor mollit minim amet ea. Fugiat amet mollit eu laboris minim nisi laborum mollit consectetur mollit voluptate. Labore irure nulla sint in Lorem elit exercitation incididunt occaecat proident labore commodo. Laboris dolore laboris voluptate amet enim duis ut id quis et deserunt enim excepteur. Ex fugiat veniam ex Lorem cillum anim occaecat dolor culpa ad. Elit consectetur sit dolor fugiat irure nisi pariatur enim duis magna deserunt consequat id labore. Veniam non reprehenderit Lorem deserunt ex deserunt sit sunt sunt proident.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2016-01-19T06:41:46 -04:00",
    "current_owner": "Quinn Lott"
  },
  {
    "label": "ad consequat",
    "description": "Reprehenderit nulla in nulla velit consectetur consectetur adipisicing occaecat sunt irure. Eu consequat pariatur sit proident. Anim do aliqua officia duis labore occaecat id aute id irure amet aliqua fugiat ullamco. Eiusmod elit est fugiat do pariatur incididunt labore irure veniam do commodo amet.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2015-11-05T05:57:32 -04:00",
    "current_owner": "Juana Owens"
  },
  {
    "label": "ullamco labore",
    "description": "Deserunt amet enim elit laboris pariatur aliqua voluptate. Labore magna do ea officia consectetur. Velit esse consequat mollit consequat cupidatat anim sit ea cillum ipsum. Eiusmod irure ipsum ullamco velit exercitation nisi adipisicing incididunt duis do.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2014-10-10T04:08:28 -04:00",
    "current_owner": "Kathrine Morin"
  },
  {
    "label": "elit id",
    "description": "Non adipisicing tempor et cupidatat aliquip non. Id culpa voluptate nulla est officia aliquip magna labore ipsum officia ad in. In aliqua anim aliquip reprehenderit. Et est duis exercitation labore esse esse. Aliqua officia exercitation aliqua veniam cillum excepteur culpa et esse in reprehenderit tempor duis sunt.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2018-08-10T01:45:03 -04:00",
    "current_owner": "Leblanc Lambert"
  },
  {
    "label": "occaecat deserunt",
    "description": "Mollit tempor do eiusmod aliquip exercitation. Aliquip adipisicing velit ad in ipsum eiusmod nostrud et aliqua proident. Aliqua id esse quis duis commodo sint dolor dolore excepteur tempor cupidatat dolore.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2018-08-07T09:39:14 -04:00",
    "current_owner": "Petra Curtis"
  },
  {
    "label": "nulla laborum",
    "description": "Tempor aliquip dolore ullamco eu ut commodo ipsum enim Lorem. Deserunt labore nisi adipisicing aliqua proident eiusmod velit Lorem irure. Anim in pariatur esse aliqua exercitation sit irure duis amet magna irure dolor pariatur anim. Mollit exercitation et exercitation aute veniam labore ea nulla consectetur excepteur irure. Dolor ut commodo do aliquip. Mollit enim aliqua magna sint mollit laborum nisi quis ex.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2015-09-29T01:33:11 -04:00",
    "current_owner": "Robbie Baird"
  },
  {
    "label": "proident pariatur",
    "description": "Velit proident mollit pariatur duis deserunt eu minim velit adipisicing sit fugiat fugiat dolor magna. Dolor aliqua adipisicing dolor qui adipisicing consectetur duis. Culpa ex voluptate incididunt mollit fugiat. Consectetur sint aliqua enim do elit mollit magna dolore nisi consequat dolore nostrud laborum.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2016-03-01T09:37:49 -04:00",
    "current_owner": "Solomon Fletcher"
  },
  {
    "label": "magna voluptate",
    "description": "Duis laboris culpa ut incididunt consequat. Nisi Lorem sit qui laborum. Consectetur nostrud ea aliqua consectetur.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2015-01-06T12:25:08 -04:00",
    "current_owner": "Isabelle Jackson"
  },
  {
    "label": "cupidatat est",
    "description": "Dolor ex elit elit incididunt fugiat. Exercitation labore exercitation eu occaecat elit exercitation et culpa ullamco consectetur officia nulla deserunt officia. Quis dolor aliqua voluptate proident et irure ipsum ea cupidatat incididunt. Et aliquip non et eu duis nulla pariatur esse. Pariatur dolor aliqua adipisicing dolor ex amet. Fugiat Lorem laboris occaecat esse tempor id quis nisi nostrud elit id. Mollit id est ut consequat.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2016-12-08T03:36:20 -04:00",
    "current_owner": "Burch Roth"
  },
  {
    "label": "voluptate dolore",
    "description": "Amet sit occaecat adipisicing aute amet mollit aliquip veniam ex deserunt incididunt. Tempor irure nulla dolor in quis dolore irure cupidatat ex labore anim duis consequat magna. Reprehenderit velit non eiusmod est ullamco enim officia culpa Lorem ad sint incididunt deserunt. Laboris nulla laborum aliquip veniam reprehenderit qui ut aliqua commodo nisi.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2014-07-26T07:49:30 -04:00",
    "current_owner": "Sherman Cooke"
  },
  {
    "label": "deserunt consequat",
    "description": "Ea est sit dolore incididunt ut consectetur voluptate ad sit cupidatat. Qui incididunt anim pariatur occaecat culpa elit laboris id velit veniam. Do et qui incididunt commodo irure. Ut tempor esse irure non laborum consequat et sunt aliquip anim exercitation. Excepteur duis proident laboris reprehenderit amet ad ad elit. Adipisicing anim adipisicing aliquip et exercitation ipsum amet veniam nulla est reprehenderit.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2014-10-14T03:42:28 -04:00",
    "current_owner": "Giles Black"
  },
  {
    "label": "laboris sint",
    "description": "Dolore ut eu voluptate id. Qui id nostrud Lorem minim minim. Nisi tempor minim consectetur ex cillum id sunt nulla magna amet minim enim. Proident ut exercitation cillum ipsum occaecat sint et ut. Anim deserunt enim amet aute. Officia magna officia qui labore exercitation Lorem consequat elit velit non in. Cillum excepteur nisi ullamco occaecat cillum.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2018-08-04T04:51:25 -04:00",
    "current_owner": "Floyd Howell"
  },
  {
    "label": "cupidatat commodo",
    "description": "Labore elit anim occaecat ipsum eiusmod dolore dolore est. Aliquip eu veniam sunt ad esse aliquip fugiat. Do Lorem labore consectetur consequat. Quis id cupidatat excepteur et. Quis qui mollit duis ut dolore elit incididunt fugiat voluptate. Fugiat non id sit laboris dolore culpa ea ad irure officia. Esse nisi in ex aliquip.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2017-11-25T06:26:23 -04:00",
    "current_owner": "Fischer Terry"
  },
  {
    "label": "adipisicing reprehenderit",
    "description": "Consequat ut voluptate culpa enim est non nulla fugiat pariatur cillum nulla. Officia aute sunt irure irure officia minim officia deserunt duis sunt aliquip reprehenderit. Ea consectetur excepteur sint do reprehenderit voluptate. Sint enim occaecat ipsum quis irure mollit ipsum laboris.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2015-06-15T05:13:34 -04:00",
    "current_owner": "Florine Castro"
  },
  {
    "label": "sunt commodo",
    "description": "Est id sit sint amet sint do. Occaecat excepteur nostrud pariatur eiusmod magna id. Aute dolore ea et sint Lorem cupidatat.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2015-06-02T10:55:26 -04:00",
    "current_owner": "Helene Contreras"
  },
  {
    "label": "ut qui",
    "description": "Mollit dolore in esse Lorem sunt dolor cupidatat exercitation ea in deserunt officia duis. Et ex laborum aute voluptate sint do ad duis velit dolor anim qui mollit. Ex et nostrud velit aliqua nisi ullamco eiusmod laboris ipsum non elit nisi cillum. Dolor adipisicing proident ullamco minim incididunt officia fugiat in eu culpa aliquip tempor est. Nostrud commodo esse voluptate voluptate. Ad qui proident ut nisi ut dolor dolor consequat consectetur ea sunt veniam nulla elit. Aliqua sunt ea commodo incididunt.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2018-01-12T04:54:34 -04:00",
    "current_owner": "Mcclure Cantu"
  },
  {
    "label": "non velit",
    "description": "Cillum adipisicing sint ullamco cillum exercitation pariatur et amet aliquip excepteur cupidatat reprehenderit cillum exercitation. Nisi ex excepteur ut dolor dolor amet id eu consequat proident consectetur. Ex proident quis ut cupidatat pariatur mollit laboris sit amet cupidatat. Exercitation aliquip exercitation eu veniam eu consequat est. Irure et reprehenderit excepteur tempor.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2016-02-27T07:58:30 -04:00",
    "current_owner": "Serena Wagner"
  },
  {
    "label": "eiusmod do",
    "description": "Tempor amet culpa esse esse tempor ut cupidatat eiusmod ad nulla. Irure adipisicing proident minim deserunt sint minim officia proident commodo sint adipisicing fugiat nostrud. Magna ad Lorem officia reprehenderit exercitation eu in commodo est aliquip exercitation pariatur reprehenderit. Consequat est elit esse eu sint est deserunt.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2014-04-10T02:12:08 -04:00",
    "current_owner": "Georgina Abbott"
  },
  {
    "label": "ullamco adipisicing",
    "description": "Ut magna qui duis aute duis et occaecat proident qui consectetur. Nulla elit cillum nostrud anim in non consequat tempor incididunt officia. Cillum dolor ullamco proident in aliquip magna officia in cillum ea laborum dolor ea. Laboris commodo do laboris ea adipisicing excepteur nostrud.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2014-02-11T07:46:41 -04:00",
    "current_owner": "Park Michael"
  },
  {
    "label": "officia exercitation",
    "description": "Veniam ex proident amet duis magna Lorem duis labore consectetur. Officia amet sint eu aliqua culpa cupidatat mollit sunt dolor. Culpa excepteur eiusmod in ad laborum laboris qui Lorem duis cupidatat fugiat. Tempor proident aute id fugiat consectetur nulla minim sunt. Aute proident qui commodo nulla occaecat cillum.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2016-07-13T04:06:56 -04:00",
    "current_owner": "Lopez Guerrero"
  },
  {
    "label": "dolore ullamco",
    "description": "Mollit dolore eu aliqua ad aliquip dolore sint ad. Aliquip ullamco officia exercitation officia aliqua nostrud ut reprehenderit excepteur mollit dolore occaecat. Consectetur dolor non dolor ipsum sunt nulla nostrud proident. Duis aliquip exercitation ad exercitation nisi. Tempor mollit sint minim labore culpa occaecat. Magna enim nisi sit Lorem et velit nostrud ullamco esse. In laboris reprehenderit in ipsum quis id occaecat veniam.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2015-04-16T05:29:20 -04:00",
    "current_owner": "Marietta Bullock"
  },
  {
    "label": "occaecat occaecat",
    "description": "Qui elit velit officia et culpa eu consequat. Ullamco ullamco sint elit duis quis voluptate veniam do eu. Cillum veniam dolor occaecat consectetur minim eiusmod ut qui eiusmod. Sit esse in velit ipsum consequat aliquip duis dolor voluptate eiusmod ad do aliqua in. Ipsum ex sint duis nulla mollit minim aute est amet mollit.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2017-02-24T01:15:29 -04:00",
    "current_owner": "Mejia George"
  },
  {
    "label": "esse velit",
    "description": "Dolore exercitation occaecat Lorem laborum consectetur anim sint anim esse. Consequat dolor in non esse id nisi dolore. Occaecat aute sint nulla ullamco voluptate quis. Pariatur in officia officia est et consectetur non ipsum irure mollit dolor aute laborum.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2018-03-20T10:25:59 -04:00",
    "current_owner": "Cassandra May"
  },
  {
    "label": "veniam sunt",
    "description": "Nulla culpa incididunt enim elit dolor. Est officia exercitation cupidatat amet aliquip reprehenderit quis minim fugiat duis et cupidatat ea. Labore tempor dolore anim commodo consectetur adipisicing eu adipisicing ea. Lorem irure amet cillum cillum nulla laborum do veniam. Ea Lorem non deserunt consectetur cillum quis pariatur exercitation reprehenderit.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2016-05-20T12:36:51 -04:00",
    "current_owner": "Tami Carr"
  },
  {
    "label": "cupidatat do",
    "description": "Elit elit dolor minim dolor proident et cupidatat exercitation Lorem quis. Ipsum commodo consequat proident enim duis dolore. Sint et qui eiusmod nostrud ipsum qui laboris minim sint consequat elit sunt ad. Laboris esse in esse sit veniam labore aliquip nulla. Ad enim ut minim nostrud. Voluptate ipsum enim tempor anim consectetur nisi dolore minim. Sunt cupidatat consequat est ullamco ipsum eu dolore quis ex.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2014-11-27T12:58:57 -04:00",
    "current_owner": "Corrine Wise"
  },
  {
    "label": "elit cillum",
    "description": "Officia quis ullamco dolor proident excepteur dolore. Fugiat Lorem mollit cillum nisi ea ut veniam quis labore ullamco. Qui aliquip irure proident exercitation nulla consequat fugiat voluptate quis sunt id magna.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2018-02-02T06:15:48 -04:00",
    "current_owner": "Sargent Burke"
  },
  {
    "label": "adipisicing labore",
    "description": "Et in pariatur velit ut duis magna consequat ad incididunt consequat. Aliquip eu sit culpa mollit exercitation ullamco mollit laboris fugiat ex et magna reprehenderit. Deserunt proident laboris cillum anim consectetur consequat aliqua adipisicing nostrud ut ut.\r\n",
    "type": "MOBILE",
    "status": "IN-USE",
    "purhcased_date": "2017-11-19T07:04:04 -04:00",
    "current_owner": "Elma Bray"
  },
  {
    "label": "aliquip elit",
    "description": "Laboris nisi aliquip sit laboris nostrud adipisicing commodo eiusmod dolore aute ad veniam voluptate. Nostrud sint non quis ea. Veniam tempor minim id esse dolore id aliquip adipisicing eu excepteur dolore elit. Labore commodo enim Lorem incididunt Lorem laboris consequat aliquip consectetur ullamco nostrud. Quis elit anim proident do et laborum irure ullamco dolor reprehenderit laborum ex id esse. Proident incididunt aliqua mollit irure mollit labore sunt pariatur quis culpa elit fugiat. Sunt excepteur est labore laboris id.\r\n",
    "type": "IPAD",
    "status": "AVAILABLE",
    "purhcased_date": "2014-12-17T02:37:23 -04:00",
    "current_owner": "Grimes Roberts"
  },
  {
    "label": "officia pariatur",
    "description": "Reprehenderit minim eiusmod dolore Lorem. Lorem cupidatat ullamco quis labore sit in nostrud pariatur. Deserunt in do eu anim nostrud id in occaecat adipisicing ad irure amet id anim. Id exercitation velit in tempor.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2015-06-19T06:18:09 -04:00",
    "current_owner": "Ashley Conner"
  },
  {
    "label": "anim anim",
    "description": "Ex qui consectetur enim cupidatat. Lorem enim ipsum Lorem quis elit est reprehenderit ipsum sit deserunt. Sint exercitation reprehenderit id adipisicing. Aliquip excepteur est nulla consequat commodo reprehenderit minim enim tempor.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2014-11-16T06:10:03 -04:00",
    "current_owner": "Katherine Benson"
  },
  {
    "label": "elit labore",
    "description": "Exercitation esse commodo pariatur non in do. Laboris anim ea esse duis ut amet dolor do laborum id. Anim pariatur non incididunt adipisicing esse do. Id cillum ullamco enim officia sit non esse est.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2017-01-29T05:11:24 -04:00",
    "current_owner": "Payne Watson"
  },
  {
    "label": "nostrud exercitation",
    "description": "Laboris pariatur deserunt sunt elit deserunt consequat mollit ad est occaecat magna sunt ad cillum. Id pariatur id anim laborum ipsum nostrud sunt ullamco tempor. Veniam exercitation do fugiat voluptate culpa ea excepteur nisi esse.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2014-10-30T11:21:53 -04:00",
    "current_owner": "Tia Reilly"
  },
  {
    "label": "sunt adipisicing",
    "description": "Aliquip ut excepteur voluptate occaecat commodo irure commodo dolor fugiat cupidatat veniam est deserunt elit. Excepteur dolore aute est ad velit non velit sint dolore dolore sunt esse. Quis esse laboris officia aute commodo qui ea reprehenderit voluptate dolor cupidatat dolor. Reprehenderit laboris sit voluptate qui sunt aliquip est veniam. Pariatur consectetur non non voluptate incididunt mollit ad anim exercitation est amet. Sit do exercitation aute ut fugiat. Ad ipsum est ex mollit eiusmod nulla deserunt.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2014-06-12T03:46:41 -04:00",
    "current_owner": "Stephens Chen"
  },
  {
    "label": "occaecat officia",
    "description": "Magna esse ut pariatur eu magna duis ea tempor in voluptate. Adipisicing minim eiusmod irure pariatur nisi deserunt id voluptate. Reprehenderit quis cillum consectetur irure voluptate ut ad laboris est consequat. Non aute anim adipisicing fugiat sunt fugiat officia ea anim esse culpa aute anim.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2018-07-31T07:35:57 -04:00",
    "current_owner": "Andrews Stafford"
  },
  {
    "label": "ipsum est",
    "description": "Adipisicing adipisicing magna nostrud nisi magna. Veniam nostrud elit ex nulla Lorem culpa quis qui. Duis aliquip dolore officia mollit labore culpa amet aute minim officia veniam voluptate adipisicing. Id velit eu irure laboris voluptate velit. Aliqua fugiat exercitation quis est deserunt fugiat non amet et sit incididunt mollit. Ipsum mollit dolore consequat officia nulla amet irure mollit et cillum ea velit.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2018-01-06T05:52:52 -04:00",
    "current_owner": "Caldwell Tyson"
  },
  {
    "label": "cupidatat irure",
    "description": "Qui nulla occaecat officia quis cupidatat sint aute cupidatat Lorem pariatur sunt ipsum. Nulla est cupidatat amet veniam dolore voluptate occaecat ad veniam non. Magna aliquip aliqua est pariatur sint adipisicing tempor do. Sunt id id deserunt est pariatur et ullamco aliqua nulla amet id enim. Aliqua est pariatur pariatur non anim. Reprehenderit elit ut labore duis amet irure amet exercitation.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2015-06-06T01:55:41 -04:00",
    "current_owner": "Sharron Dotson"
  },
  {
    "label": "deserunt deserunt",
    "description": "Nostrud irure consequat voluptate deserunt Lorem ad amet excepteur. Veniam sint minim velit tempor. Adipisicing esse exercitation deserunt velit est.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2018-04-19T08:45:36 -04:00",
    "current_owner": "Inez Hines"
  },
  {
    "label": "elit occaecat",
    "description": "Lorem excepteur Lorem ipsum consequat. Incididunt labore occaecat dolor proident ut exercitation aute nostrud deserunt. Ullamco aute ad deserunt sit eiusmod ullamco in eiusmod. Et commodo velit ipsum voluptate laborum nulla. Cillum aute reprehenderit et Lorem ex amet enim aliquip. Enim mollit est nulla tempor veniam eiusmod ad ex in sint.\r\n",
    "type": "MOBILE",
    "status": "AVAILABLE",
    "purhcased_date": "2018-05-21T06:00:38 -04:00",
    "current_owner": "Guadalupe Rush"
  },
  {
    "label": "tempor excepteur",
    "description": "Laborum et esse qui elit mollit aute deserunt velit esse pariatur ex commodo elit ullamco. Sunt aute deserunt est ipsum voluptate voluptate sit nisi occaecat esse aliquip ullamco ad nulla. Cupidatat fugiat anim magna fugiat qui aute commodo non sit consectetur dolor ut culpa. Irure laborum ut deserunt aliquip do adipisicing ex anim est nisi ex cupidatat.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2014-04-22T03:09:28 -04:00",
    "current_owner": "Teri Burnett"
  },
  {
    "label": "ad reprehenderit",
    "description": "Cillum officia id nostrud ea aliqua quis incididunt nisi nostrud sint est ad dolore. Fugiat duis aute pariatur laboris do laborum Lorem amet. Consectetur cupidatat minim commodo tempor. Amet nostrud laboris do elit.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2015-11-26T07:54:22 -04:00",
    "current_owner": "Sharlene Lewis"
  },
  {
    "label": "qui incididunt",
    "description": "Anim mollit amet nisi pariatur elit labore consectetur reprehenderit irure. Consequat laborum irure voluptate consequat proident aliqua cillum aliqua laborum ullamco. Irure cupidatat officia occaecat magna. Eu sit tempor laborum laborum aute proident ipsum anim adipisicing sit. Incididunt laborum excepteur enim ut et.\r\n",
    "type": "LAPTOP",
    "status": "IN-USE",
    "purhcased_date": "2018-05-16T05:29:00 -04:00",
    "current_owner": "Catalina Tillman"
  },
  {
    "label": "ea qui",
    "description": "Irure voluptate aliqua eiusmod pariatur deserunt pariatur. Ad eiusmod anim mollit culpa sunt incididunt pariatur sint ullamco do excepteur anim sint cupidatat. Aute nulla velit anim minim deserunt nulla cupidatat nulla officia fugiat reprehenderit dolor.\r\n",
    "type": "IPAD",
    "status": "IN-USE",
    "purhcased_date": "2014-11-15T05:40:31 -04:00",
    "current_owner": "Bender Potter"
  },
  {
    "label": "nisi duis",
    "description": "Esse do in dolor ad exercitation quis eu nostrud elit reprehenderit incididunt ex consectetur. Id ad sit laboris et labore fugiat in deserunt laboris. Ut dolore duis do amet pariatur nulla ut esse enim. Aliquip Lorem aliquip et dolore excepteur. Minim reprehenderit exercitation officia aliqua nisi aliqua nostrud magna duis reprehenderit. Irure ut aute id occaecat sint proident incididunt velit id dolor quis consequat exercitation. Qui consectetur nisi qui et dolore aute quis qui.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2015-02-09T07:18:22 -04:00",
    "current_owner": "Candy Lester"
  },
  {
    "label": "veniam mollit",
    "description": "Exercitation pariatur aliquip occaecat tempor cupidatat sunt excepteur nulla pariatur irure eiusmod. Commodo officia reprehenderit ut officia esse deserunt labore ipsum. Aliqua ex culpa consequat mollit est qui ullamco veniam fugiat. Veniam proident commodo consequat elit consequat et ad. Deserunt sit ex anim mollit eu amet.\r\n",
    "type": "LAPTOP",
    "status": "AVAILABLE",
    "purhcased_date": "2017-02-15T05:40:27 -04:00",
    "current_owner": "Lindsey Clayton"
  }
]