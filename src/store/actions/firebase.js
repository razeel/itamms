import 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import { push } from 'connected-react-router';
import { auth, db } from '../../firebase';
import RoutesConfig from '../../config/routes';
import { SNAPSHOT_REFS } from '../../config/firebase';

// Login action and redirect to dashboard page
export const doLogin = (email, password, isRedirect) => async (dispatch) => {
  await auth.signInWithEmailAndPassword(email, password)
    .then( ({user}) => { 
      dispatch({ type: 'AUTH_SUCCESS', payload: { email: user.email, uid: user.uid }});
      if(isRedirect) dispatch(push(RoutesConfig.DASHBOARD));
    })
    .catch( (error) => { 
      dispatch({ type: 'LOGIN_ERROR', payload: error.message });
     });
}

// Logout action and redirect to login page
export const doLogout = (redirectTo) => async (dispatch) => {
  await auth.signOut();
  dispatch({ type: 'AUTH_FAILED' });
  dispatch(push(redirectTo));
}

// Check auth
export const checkAuth = () => async (dispatch) => {
  dispatch({ type: 'AUTH_CHECKING' });
  await auth.onAuthStateChanged( user => {
    if(user) {
      dispatch({ type: 'AUTH_SUCCESS', payload: { email: user.email, uid: user.uid }});
    } else {
      dispatch({ type: 'AUTH_FAILED' });
    } 
  })
}

// Get assets
export const getAssets = () => async(dispatch) => {

  dispatch({ type: 'LOADING_ASSETS' });

  const assetsRef = db.ref(SNAPSHOT_REFS.ASSETS);
  assetsRef.on('value', (snapshot) => {
      const payload = snapshot.val();
      dispatch({ type: 'SUCCESS_LOADING_ASSETS', payload });
  }, (error) => {
      dispatch({ type: 'FAILED_LOADING_ASSETS', payload: 'Error in loading assets' });
  });

};