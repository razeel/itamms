import 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import { map } from 'lodash';
import { auth, db } from '../../firebase';
import { SNAPSHOT_REFS } from '../../config/firebase';

import sampleData from '../data/assets.sample';


// install demo data
export const installSampleData = () => async (dispatch) => {
  
  const assetsRef = db.ref(SNAPSHOT_REFS.ASSETS);
  const toggles = db.ref('/toggles');

  toggles.once('value').then(snapshot => {
    if( !snapshot.hasChild('isSampleDataExists') || (snapshot.hasChild('isSampleDataExists') && !snapshot.child('isSampleDataExists').val()) ) {

        map(sampleData, data => {
          let newAssetRef = assetsRef.push();
          newAssetRef.set(data);
        });

        let updates = {};
        updates['isSampleDataExists'] = true;
        toggles.update(updates);


        dispatch({ type: 'SAMPLE_DATA_EXISTS' });

    } else {

      dispatch({ type: 'SAMPLE_DATA_EXISTS' });
      
    }
  })
}