export const REGISTER = '/register';
export const LOGIN = '/login';
export const DASHBOARD = '/';
export const ACCOUNT = '/account';
export const FORGOTTEN = '/forgotten';
export const DEMO = '/install-sample-data';

export default { REGISTER, LOGIN, DASHBOARD, ACCOUNT, FORGOTTEN, DEMO };