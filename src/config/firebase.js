export const API_KEY =  'AIzaSyCl6pv_MQzlHuQ11zKH0F77wuM3vRyciO4';
export const AUTH_DOMAIN = 'dev-sandbox-166719.firebaseapp.com';
export const DATABASE_URL = 'https://dev-sandbox-166719.firebaseio.com';
export const PROJECT_ID = 'dev-sandbox-166719';
export const STORAGE_BUCKET = 'dev-sandbox-166719.appspot.com';
export const MESSAGING_SENDER_ID = '584776813332';

export const SNAPSHOT_REFS = {
  ASSETS: '/assets',
  MASTERS: '/masters',
  USERS: '/profiles'
}

export default { API_KEY, AUTH_DOMAIN, DATABASE_URL, PROJECT_ID, STORAGE_BUCKET, MESSAGING_SENDER_ID, SNAPSHOT_REFS };