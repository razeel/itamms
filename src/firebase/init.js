import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import FIREBASE_CONFIG from '../config/firebase';

var config = {
  apiKey: FIREBASE_CONFIG.API_KEY,
  authDomain: FIREBASE_CONFIG.AUTH_DOMAIN,
  databaseURL: FIREBASE_CONFIG.DATABASE_URL,
  projectId: FIREBASE_CONFIG.PROJECT_ID,
  storageBucket: FIREBASE_CONFIG.STORAGE_BUCKET,
  messagingSenderId: FIREBASE_CONFIG.MESSAGING_SENDER_ID
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const db = firebase.database();

export default { auth, db };