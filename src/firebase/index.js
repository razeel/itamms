import * as firebase from './init';

export const db = firebase.db;
export const auth = firebase.auth;
